<?php 
SESSION_START();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- site icons -->
      <link rel="icon" href="images/fevicon/logo.png" type="image/png" />
      <!-- bootstrap css -->
      <link rel="stylesheet" href="css/bootstrap.min.css" />
      <!-- site css -->
      <link rel="stylesheet" href="css/stay.css" />
      <!-- responsive css -->
      <link rel="stylesheet" href="css/responsive.css" />
      <!-- colors css -->
      <link rel="stylesheet" href="css/colors.css" />
      <!-- wow animation css -->
      <link rel="stylesheet" href="css/animate.css" />
      <link rel="stylesheet" href="style/dashboard1.css">
    <title>Dashboard</title>
</head>
<body>
<header class="">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-lg-12">
                  <div class="logo"><a href="index.html"><img src="images/Logo_RSIA_PuriBunda.png" alt="#" width="100px" height="150px" /></a></div>
               </div>
               <div class="col-md-2 col-lg-2">
               <div align="left" style="margin-left:34px; color:black; font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"> Welcome <?php 
                  echo $_SESSION['nama'];
                  ?>
                  <link rel="stylesheet" href="halo.css">
                  </div>
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"> <a class="bt_main" href="dashboard.php">Dashboard</a></div>
                  <?php if ($_SESSION['ROLE'] == 'Kasir') { ?>
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"><a class="bt_main" href="sign_biling.php">Sign Billing</a></div>
                     <?php } ?>
                     <?php if ($_SESSION['ROLE'] == 'Apotekker') { ?>
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"><a class="bt_main" href="sign_resep.php">Sign Resep</a></div>
                     <?php } ?>

                     <?php if ($_SESSION['ROLE'] == 'Admin') { ?>
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"><a class="bt_main" href="sign_biling.php">Sign Billing</a></div>
                     <?php } ?>
                     <?php if ($_SESSION['ROLE'] == 'Admin') { ?>
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"><a class="bt_main" href="sign_resep.php">Sign Resep</a></div>
                     <?php } ?>
                  <?php if ($_SESSION['ROLE'] == 'Admin') { ?>
                     <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"> <a class="bt_main" href="contact.php">Master </a></div>
                  <?php } ?>
                  <!-- <div class="dropdown show">
                  <button class="bt_main" style="margin-left:20px" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     Laporan
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                     <a class="dropdown-item" href="sign_biling.php">Laporan Biling</a>
                     <a class="dropdown-item" href="sign_resep.php">Laporan Resep</a>
                  </div>
                  </div>  -->
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"> <a class="bt_main" href="logout.php">Log Out</a> </div>
               </div>
               <div class="button">
                     <h2>WELCOME</h2>
                     <h5>To E-Biling Application</h5>
                     <img src="images/ibuanak.png" width="700px" height="380px" />
                </div>
</body>
      <!-- end header -->
      <!--=========== js section ===========-->
      <!-- jQuery (necessary for Bootstrap's JavaScript) -->
      <script src="js/jquery.min.js"></script>
      <script src="js/popper.min.js"></script>
      <script src="js/bootstrap.min.js"></script>

      <!-- wow animation -->
      <script src="js/wow.js"></script>
      <!-- custom js -->
      <script src="js/custom.js"></script>
</html>