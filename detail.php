<?php
    SESSION_START();
    require 'config/index.php';
    use GuzzleHttp\Client;
    $client = new Client();

    $response = $client->get('http://116.0.2.210:8080/sanata-api/public/api/invoice/take-invoice?no_bukti='.$_GET['no_bukti']);
    $data = $response->json();
    include "phpqrcode/qrlib.php"; 

    $tempdir = "temp/"; //Nama folder tempat menyimpan file qrcode
    if (!file_exists($tempdir)) //Buat folder bername temp
        mkdir($tempdir);

        //isi qrcode jika di scan
        $codeContents = 'http://192.168.1.239/project-biling/Biling/'.$_GET['no_bukti'].".pdf"; 
    
    //simpan file kedalam temp 
    //nilai konfigurasi Frame di bawah 4 tidak direkomendasikan 
        QRcode::png($codeContents, $tempdir.'008_4.png', QR_ECLEVEL_L, 2, 3);    

        // echo '<h2>Ukuran Frame QRCode</h2>';
        // // displaying 
        // echo '<img src="'.$tempdir.'008_4.png" />'; 
?>
<html>
<head>
    <link rel="stylesheet" href="E-Biling/style1.css"><title> E-BILLING </title>
    <link rel="icon" href="images/fevicon/logo.png" type="image/png" />
</head>
<body>
    <!-- <style>
        body {
            height: 842px;
            width: 595px;
            /* to centre page on screen*/
            margin-left: auto;
            margin-right: auto;
        }
        
    </style> -->
    <section>
        <div class="container">
            <img src="E-Biling/logo.png" alt="" width="65px" height="65px">
        </div>
      
         
         <div class="home__judul">
            <h1>RSIA Puri Bunda</h1>
        </div>
         <div class="home__alamat">
            <p>Jln. Gatot Subroto VI / 19 Denpasar, Bali</p>
        </div>
        </div>
        <div class="home__tlp">
            <p>Telp. (0361) 437999</p>
            </div> 
        <div class="inap">
        <h1 align="center">INVOICE RAWAT JALAN</h1>
         <br> 
        &nbsp;
        <hr>
        <table class="tabel1" border="0">
        <tr>
            <td>NO. REGISTER </td>
            <td>:</td>
            <td style="font-family:Times; font-weight:bold;"> <?php echo $data['data']['pasien']['no_reg']; ?></td>
            <td>TANGGAL</td>
            <td>:</td>
            <td> <?php echo $data['data']['pasien']['tgl_bukti']; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $data['data']['pasien']['jam_bukti']; ?></td>
        </tr>
        <tr>
            <td>NRM / CM </td>
            <td>:</td>
            <td style="font-family:Times; font-weight:bold;"> <?php echo $data['data']['pasien']['nrm']; ?></td>
            <td>DOKTER</td>
            <td>:</td>
            <td> <?php echo $data['data']['pasien']['nama_dokter'] ?></td>
        </tr>
        <tr>
            <td>NAMA </td>
            <td>:</td>
            <td style="font-weight:bold; font-family:Times,sans;"> <?php echo $data['data']['pasien']['nama_pasien']; ?></td>
            <td>DIAGNOSA</td>
            <td>:</td>
            <td> <?php echo $data['data']['invoice'][0]['diagnosa']; ?></td>
        </tr>
        <tr>
            <td>Umur / JENIS. K </td>
            <td>:</td>
            <td> <?php echo $data['data']['pasien']['umur_thn']; ?> &nbsp;/&nbsp;&nbsp;&nbsp;<?php echo $data['data']['pasien']['jenis_kelamin']; ?></td>
            <td>JENIS PASIEN</td>
            <td>:</td>
            <td> <?php echo $data['data']['pasien']['tipe_pasien']; ?></td>
        </tr>
    </table>
    <hr>
    <div class="bagian2">
        <h1 align="center" style="font-style: italic; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif; font-size: 17px; margin-top:2px;"><?php echo $data['data']['pasien']['no_bukti']; ?></h1>
    </div>
    <br><br>
    <table class="tabel1" border="0">
        <tr class="rincian">
            <th><h4>RINCIAN BIAYA</h4></th>
        </tr>
    </table><br>
    &nbsp;&nbsp;
    <br>
        <table class="tabel8" border="0" width="890px">
        <?php
        $total = 0;
        $hasil = 0;
        foreach ($data['data']['invoice'] as $jasa){
            $total=($jasa['qty'] * $jasa['nilai'])+$total;
            // $hasil=($jasa['nilai'] * $jasa['qty'])+ $hasil;
        ?>
            <tr>
                <td><?php echo $jasa['nama_jasa']; ?></td>
                <td><?php echo $jasa['qty'] ?> </td>
                <td> x </td>
                <td align="right">Rp. </td>
                <td align="right"><?php echo number_format($jasa['nilai'], 0, '', '.'); ?> </td>
                <!-- <td><?php echo $jasa['discount'] ?></td> -->
                
            </tr>
            
        <?php
        }
        ?> 
          
        <hr style="width: 150px; margin-left: 740px;"> 
        <p style="margin-left:900px; margin-top:-5px">(+)</p>
    </table>
    <!-- <table class="tabel0" border="0">
        <tr>
            <td style="margin-top:6px;">JUMLAH =</td>
        </tr>
    </table> -->
    <h5 style="font-size:12px; margin-left:500px;">JUMLAH = </h5>
    <table class="tabel9" border="0" width="700px">
        <tr>
            <td>Rp.</td>
            <td align="right"><?php echo number_format($total,0,'','.'); ?></td>
        </tr>
        
    </table><br><br><hr>   
    <table class="tabel4" border="0" width="750px" height="10px">
        <tr>
            <th align="left"><h4>Terbilang: <br><?php echo $data['data']['pasien']['terbilang_grand_total']; ?></h4></th>
            <!-- <th align="right">Denpasar, <?php echo $data['data']['pasien']['tgl_bukti']; ?><th> 
            <th>Kasir<th> -->
    
        <tr>
    </table><br>
        <div class="sistem">
            
             <?php $tanggal= mktime(date("m"),date("d"),date("Y"));
                echo"Denpasar".' '.  date('d F Y');?>&nbsp;
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kasir</p>
        </div>
        <div class="bawah" align="right">
        <br><br><br><br><br><br><br><br>
        <br><br><br><br><br>
        <?php
        echo '<img src="'.$tempdir.'008_4.png" />';
        ?>
        <h4 style="font-size:12px;"><?php echo $_SESSION['nama'];?></h4>
       </div>
        </table><br>
        <div class="nama">
        <p>(Pasien/Keluarga)</p>
    </div>
        
    <div class="ttd">
        <br><br><br>
        <h4><?php echo $data['data']['pasien']['nama_pasien']; ?></h4>
    </div>
    </table>
        
  
         
  

</body>
</html>
