<?php
SESSION_START();
include 'koneksi.php';
require 'config/index.php';
use GuzzleHttp\Client;
$client = new Client();
$no_resep='';
if(isset($_GET['no_resep'])){
    $no_resep = $_GET['no_resep'];
}
$response = $client->get('http://116.0.2.210:8080/sanata-api/public/api/resep/take-resep?no_resep='.$no_resep);
$data = $response->json();
$reservasi = $data['data'];
// var_dump($reservasi);
?>
<?php 

$query = mysqli_query($connect, "SELECT *,reseppdf.status as status_kirim1 FROM reseppdf  
LEFT JOIN karyawan
ON reseppdf.pengirim = karyawan.id
WHERE no_resep = '$no_resep'") or die (mysqli_error($connect));
$resep = mysqli_fetch_array($query);
$status = $resep['status_kirim1'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- site icons -->
      <link rel="icon" href="images/fevicon/logo.png" type="image/png" />
      <!-- bootstrap css -->
      <link rel="stylesheet" href="css/bootstrap.min.css" />
      <!-- site css -->
      <link rel="stylesheet" href="css/stay.css" />
      <!-- responsive css -->
      <link rel="stylesheet" href="css/responsive.css" />
      <!-- colors css -->
      <link rel="stylesheet" href="css/colors.css" />
      <!-- wow animation css -->
      <link rel="stylesheet" href="css/animate.css" />
      <link rel="stylesheet" href="dashboard.css">
      <link rel="stylesheet" href="style/signcss.css">
      <link rel="stylesheet" href="vendor/boostrap-datepicker/css/bootstrap-datepicker.min.css" />

      <!-- jQuery (necessary for Bootstrap's JavaScript) -->
      <script src="js/jquery.min.js"></script>
      <script src="js/popper.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="vendor/boostrap-datepicker/js/bootstrap-datepicker.min.js"></script>
      <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

      <!-- wow animation -->
      <script src="js/wow.js"></script>
      <!-- custom js -->
      <script src="js/custom.js"></script>
      <title>Sign Resep</title>
</head>
<body>
<header class="">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-lg-12">
                  <div class="logo"><a href="index.html"><img src="images/Logo_RSIA_PuriBunda.png" alt="#" width="100px" height="150px" /></a></div>
               </div>
               <div class="col-md-2 col-lg-2">
               <div align="left" style="margin-left:34px; color:black; font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"> Welcome <?php 
                  echo $_SESSION['nama'];
                  ?>
                  <link rel="stylesheet" href="halo.css">
                  </div>
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"> <a class="bt_main" href="dashboard.php">Dashboard</a></div>
                  <?php if ($_SESSION['ROLE'] == 'Kasir') { ?>
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"><a class="bt_main" href="sign_biling.php">Sign Billing</a></div>
                     <?php } ?>
                     <?php if ($_SESSION['ROLE'] == 'Apotekker') { ?>
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"><a class="bt_main" href="sign_resep.php">Sign Resep</a></div>
                     <?php } ?>

                     <?php if ($_SESSION['ROLE'] == 'Admin') { ?>
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"><a class="bt_main" href="sign_biling.php">Sign Billing</a></div>
                     <?php } ?>
                     <?php if ($_SESSION['ROLE'] == 'Admin') { ?>
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"><a class="bt_main" href="sign_resep.php">Sign Resep</a></div>
                     <?php } ?>
                  <?php if ($_SESSION['ROLE'] == 'Admin') { ?>
                     <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"> <a class="bt_main" href="contact.php">Master </a></div>
                  <?php } ?>
                  <div class="right_bt"> <a class="bt_main" href="logout.php">Log Out</a> </div>
               </div>
                     
               <div class="col-md-8">
               <form id="testform" action="mail-resep.php?no_resep=<?php echo $no_resep ?>" method="POST" class="col-md-6">
                     <input type="text" class="form-control" name="email" placeholder="Email Anda" value="<?php echo $data['data']['pasien']['email']; ?>" required>
                     <input type="text" class="form-control" placeholder="Nama Pasien" value="<?php echo $data['data']['pasien']['nama_pasien']; ?>">
                  </form>
                  <button class="btn btn-primary" type="button" id="send" name="submit">Kirim E-mail</button>
                  <button type="button" class="btn btn-red mb-2 ml-2"><a href="sign_resep.php">Back</a></button>
                  <form class="form-inline">
                  <button class="btn">status : 
                  <?php if(@$status==1){ ?>
                     <span class="badge badge-success">Terkirim</span>
                     <?php } else {?>
                     <span class="badge badge-danger">Belum terkirim</span>
                     <?php }?></button>&nbsp;
                     <button class="btn">pengirim : <?php echo @$resep['nama']; ?></button>&nbsp;
                     <button class="btn">Tanggal Kirim : <?php echo @$resep['tanggal_kirim1']; ?></button>
                  </form>
               <iframe type="application/pdf" src="generate-resep-pdf.php?no_resep=<?=$_GET['no_resep']?>" width="900" height="440"></iframe>
               </div>
             </div>
         </div>
       </div>
    </div>
</header>
</body>
    <!-- end header -->
      <!--=========== js section ===========-->
      <script>
function sweet (){
swal("Good job!");
}
</script>
      <script>
         $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true // autoclose digunakan untuk tutup otomatis setelah memilih tanggal
         });
      </script>
      <script>
         $('#myTab a').on('click', function (e) {
         e.preventDefault()
         $(this).tab('show')
         }) 
      </script>
<script>

$( "#send" ).click(function() {
   Swal.fire({
      title: 'Anda Yakin Ingin Mengirim Ke Email Ini?',
      showCancelButton: true,
      confirmButtonText: 'Kirim',
      }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
         myFunction()
      } else if (result.isDenied) {
         Swal.fire('Changes are not saved', '', 'info')
      }
   })
});
function myFunction() {
$( "form#testform" ).submit();
}
</script>
      
</html>
