<?php 
SESSION_START();
if(!isset($_SESSION['username'])) {
   header('location:http://localhost/project-biling/index1.php');
}
?>
<!DOCTYPE html>
<html>
   <head>
      <!-- basic -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>Master</title>
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- site icons -->
      <link rel="icon" href="images/fevicon/logo.png" type="image/png" />
      <!-- bootstrap css -->
      <link rel="stylesheet" href="css/bootstrap.min.css" />
      <!-- site css -->
      <link rel="stylesheet" href="css/stay.css" />
      <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap4.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
      <!-- responsive css -->
      <link rel="stylesheet" href="css/responsive.css" />
      <!-- colors css -->
      <link rel="stylesheet" href="css/colors.css" />
      <!-- wow animation css -->
      <link rel="stylesheet" href="css/animate.css" />
      <!-- <link rel="stylesheet" href="style.css"> -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body background="" id="default_theme" class="team">
      <!-- header -->
      <header class="">
         <div class="container">
            <div class="row">
               <div class="col-md-2 col-lg-2">
               <div class="logo"><center><a href="index.html"><img src="images/Logo_RSIA_PuriBunda.png" alt="#" width="100px" height="150px" /></a></center></div>
                  <div align="left" style="margin-left:34px; font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"> Welcome <?php 
                     echo $_SESSION['nama'];
                     ?>
                     <link rel="stylesheet" href="halo.css">
                  </div>
                  <div class="table-responsive">
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"> <a class="bt_main" href="dashboard.php">Dashboard</a></div>
                  <?php if ($_SESSION['ROLE'] == 'Kasir') { ?>
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"><a class="bt_main" href="sign_biling.php">Sign Billing</a></div>
                     <?php } ?>
                     <?php if ($_SESSION['ROLE'] == 'Apotekker') { ?>
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"><a class="bt_main" href="sign_resep.php">Sign Resep</a></div>
                     <?php } ?>

                     <?php if ($_SESSION['ROLE'] == 'Admin') { ?>
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"><a class="bt_main" href="sign_biling.php">Sign Billing</a></div>
                     <?php } ?>
                     <?php if ($_SESSION['ROLE'] == 'Admin') { ?>
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"><a class="bt_main" href="sign_resep.php">Sign Resep</a></div>
                     <?php } ?>
                  <?php if ($_SESSION['ROLE'] == 'Admin') { ?>
                     <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"> <a class="bt_main" href="contact.php">Master </a></div>
                  <?php } ?>
                  <!-- <div class="dropdown show">
                  <button class="bt_main" style="margin-left:10px" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     Laporan
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                     <a class="dropdown-item" href="sign_biling.php">Laporan Biling</a>
                     <a class="dropdown-item" href="sign_resep.php">Laporan Resep</a>
                  </div>
                  </div>  -->
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"> <a class="bt_main" href="logout.php">Log Out</a> </div>
                  </div>
               </div>
               <div class="col-md-10 col-lg-10">
                  <div class="button mt-5 mb-2 ml-1">
                     <center><h2>MANAJEMENT USER</h2></center>
                     <a href="input.php"><button class="btn btn-primary ">Tambah Data</button></a>
                  </div>
                  <?php
                     include 'tabel.php';
                  ?> 
                  <link rel="stylesheet" href=".css">  
               </div>
            </div>     
         </div>        
      </header>
      <!-- end header -->
      <!--=========== js section ===========-->
      <!-- jQuery (necessary for Bootstrap's JavaScript) -->
      <script src="js/jquery.min.js"></script>
      <script src="js/popper.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
      <script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
      <script src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap4.min.js"></script>
      
      <!-- wow animation -->
      <script src="js/wow.js"></script>
      <!-- custom js -->
      <script src="js/custom.js"></script>
      <script>
         $(document).ready(function() {
         $('#example').DataTable();
         } );
      </script>
   </body>
</html>