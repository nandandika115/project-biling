<?php
SESSION_START();
include 'koneksi.php';  
require 'config/index.php';
use GuzzleHttp\Client;
$client = new Client();
$no_bukti='';
if(isset($_GET['no_bukti'])){
    $no_bukti = $_GET['no_bukti'];
}
$phone='';
if(isset($_GET['phone'])){
   $phone = $_GET['phone'];
}
$response = $client->get('http://116.0.2.210:8080/sanata-api/public/api/invoice/take-invoice?no_bukti='.$no_bukti);
$data = $response->json();
$reservasi = $data['data']['pasien'];
// print_r($reservasi);
?>
<?php 

$query = mysqli_query($connect, "SELECT *,bilingpdf.status as status_kirim0 FROM bilingpdf LEFT JOIN karyawan ON bilingpdf.pengirim = karyawan.id WHERE no_bukti_biling = '$no_bukti'") or die (mysqli_error($connect));
$biling = mysqli_fetch_array($query);
$status = $biling['status_kirim0'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- site icons -->
      <link rel="icon" href="images/fevicon/logo.png" type="image/png" />
      <!-- bootstrap css -->
      <link rel="stylesheet" href="css/bootstrap.min.css" />
      <!-- site css -->
      <link rel="stylesheet" href="css/stay.css" />
      <!-- responsive css -->
      <link rel="stylesheet" href="css/responsive.css" />
      <!-- colors css -->
      <link rel="stylesheet" href="css/colors.css" />
      <!-- wow animation css -->
      <link rel="stylesheet" href="css/animate.css" />
      <link rel="stylesheet" href="dashboard.css">
      <link rel="stylesheet" href="style/signcss.css">
      <link rel="stylesheet" href="vendor/boostrap-datepicker/css/bootstrap-datepicker.min.css" />

      <!-- jQuery (necessary for Bootstrap's JavaScript) -->
      <script src="js/jquery.min.js"></script>
      <script src="js/popper.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="vendor/boostrap-datepicker/js/bootstrap-datepicker.min.js"></script>
      <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
      <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

      <!-- wow animation -->
      <script src="js/wow.js"></script>
      <!-- custom js -->
      <script src="js/custom.js"></script>
      <title>PDF Billing</title>
</head>
<body>
<header class="">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-lg-12">
                  <div class="logo"><a href="index.html"><img src="images/Logo_RSIA_PuriBunda.png" alt="#" width="100px" height="150px" /></a></div>
               </div>
               <div class="col-md-2 col-lg-2">
               <div align="left" style="margin-left:34px; color:black; font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"> Welcome <?php 
                  echo $_SESSION['nama'];
                  ?>
                  <link rel="stylesheet" href="halo.css">
                  </div>
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"> <a class="bt_main" href="dashboard.php">Dashboard</a></div>
                  <?php if ($_SESSION['ROLE'] == 'Kasir') { ?>
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"><a class="bt_main" href="sign_biling.php">Sign Billing</a></div>
                     <?php } ?>
                     <?php if ($_SESSION['ROLE'] == 'Apotekker') { ?>
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"><a class="bt_main" href="sign_resep.php">Sign Resep</a></div>
                     <?php } ?>

                     <?php if ($_SESSION['ROLE'] == 'Admin') { ?>
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"><a class="bt_main" href="sign_biling.php">Sign Billing</a></div>
                     <?php } ?>
                     <?php if ($_SESSION['ROLE'] == 'Admin') { ?>
                  <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"><a class="bt_main" href="sign_resep.php">Sign Resep</a></div>
                     <?php } ?>
                  <?php if ($_SESSION['ROLE'] == 'Admin') { ?>
                     <div class="right_bt" style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;"> <a class="bt_main" href="contact.php">Master </a></div>
                  <?php } ?>
                  <div class="right_bt"> <a class="bt_main" href="logout.php">Log Out</a> </div>
               </div>
            
               <div class="col-md-8">
                  <form id="cobaform" action="mail.php?no_bukti=<?php echo $no_bukti ?>" method="POST" class="col-md-6">
                     <input type="text" class="form-control" name="email" placeholder="Email Anda" value="<?php echo $data['data']['pasien']['email']; ?>" required>
                     <input type="text" class="form-control" placeholder="Nama Pasien" value="<?php echo $data['data']['pasien']['nama_pasien']; ?>">
                  </form>
                  <button class="btn btn-primary" type="button" id="kirim" name="submit">Kirim E-mail</button>
                  <button type="button" class="btn btn-red mb-2 ml-2"><a href="sign_biling.php">Back</a></button>
                  
                  <form class="form-inline">
                     <button class="btn">status : 
                     <?php if(@$status==1){ ?>
                     <span class="badge badge-success">Terkirim</span>
                     <?php } else {?>
                     <span class="badge badge-danger">Belum terkirim</span>
                     <?php }?></button>&nbsp;
                     <button class="btn">pengirim : <?php echo @$biling ['nama']; ?></button>&nbsp;
                     <button class="btn">Tanggal Kirim : <?php echo @$biling['tanggal_kirim']; ?></button>
                  </form>
                  <iframe type="application/pdf" src="generate-billing-pdf.php?no_bukti=<?=$_GET['no_bukti']?>" width="900" height="440"></iframe>
               </div>
            </div>
         </div>
      </div>
   </div>
         <!-- <form id="cobaform" action="mail.php?no_bukti=<?php echo $no_bukti ?>" method="POST">
            <input type="text" class="form-control" name="email" placeholder="Email Anda" value="<?php echo $data['data']['pasien']['email']; ?>" required>
            <button class="btn btn-primary" type="button" id="kirim" name="submit">Kirim E-mail</button>
            <button type="button" class="btn btn-red mb-2 ml-2"><a href="sign_biling.php">Back</a></button>
         </form> -->
         
</header>
</body>
    <!-- end header -->
      <!--=========== js section ===========-->
      
      <script>
         $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true // autoclose digunakan untuk tutup otomatis setelah memilih tanggal
         });
      </script>
      <script>
         $('#myTab a').on('click', function (e) {
         e.preventDefault()
         $(this).tab('show')
         }) 
       </script>
   <!-- <script>
      $( "#biling" ).submit(function( event ) {
  alert( "Handler for .submit(#biling) called." );
  event.preventDefault();
});
   </script> -->
<script>

   $( "#kirim" ).click(function() {
      // swal({
      //    title: "Anda Yakin Ingin Mengirim Ke Email Ini?",
      //    // text: "Once deleted, you will not be able to recover this imaginary file!",
      //    icon: "warning",
      //    buttons: true,
      //    dangerMode: true,
      //    })
      //    .then((KirimEmail) => {
      //    if (KirimEmail) {
      //       $('#form-biling').submit();
      //    } else {
      //       swal("Email Telah Berhasil Dikrim");
      //    }
      //  });
      Swal.fire({
         title: 'Anda Yakin Ingin Mengirim Ke Email Ini?',
         showCancelButton: true,
         confirmButtonText: 'Kirim',
         }).then((result) => {
         /* Read more about isConfirmed, isDenied below */
         if (result.isConfirmed) {
            myFunction()
         } else if (result.isDenied) {
            Swal.fire('Changes are not saved', '', 'info')
         }
      })
   });
//    swal({
//    title: "Are you sure?",
//    text: "Once deleted, you will not be able to recover this imaginary file!",
//    icon: "warning",
//    buttons: true,
//    dangerMode: true,
//    })
//    .then((willDelete) => {
//    if (willDelete) {
//       $( "#biling" ).submit();
//    } else {
//       swal("Your imaginary file is safe!");
//    }
// });
function myFunction() {
   $( "form#cobaform" ).submit();
}
</script>
</html>
