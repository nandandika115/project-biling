-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2022 at 09:00 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pegawai`
--

-- --------------------------------------------------------

--
-- Table structure for table `bilingpdf`
--

CREATE TABLE `bilingpdf` (
  `no_bukti_biling` varchar(50) NOT NULL,
  `pengirim` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `bilingpdf_id` int(11) NOT NULL,
  `tanggal_kirim` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bilingpdf`
--

INSERT INTO `bilingpdf` (`no_bukti_biling`, `pengirim`, `status`, `bilingpdf_id`, `tanggal_kirim`) VALUES
('211006INVRI-021733', 1, 1, 2, '2022-05-03'),
('211005INVRJ-053303', 1, 1, 4, '2022-05-03'),
('211006INVRI-021732', 1, 1, 5, '2022-05-03'),
('211005INVRJ-053298', 1, 1, 12, '2022-05-03'),
('211005INVRJ-053296', 0, 0, 13, NULL),
('211005INVRJ-053297', 0, 0, 14, NULL),
('211005INVRJ-053299', 0, 0, 15, NULL),
('211005INVRJ-053300', 0, 0, 16, NULL),
('211005INVRJ-053302', 0, 0, 17, NULL),
('211005INVRJ-053301', 0, 0, 18, NULL),
('211006INVRI-021731', 0, 0, 19, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bilingpdf`
--
ALTER TABLE `bilingpdf`
  ADD PRIMARY KEY (`bilingpdf_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bilingpdf`
--
ALTER TABLE `bilingpdf`
  MODIFY `bilingpdf_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
