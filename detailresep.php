<?php
SESSION_START();
require 'config/index.php';
use GuzzleHttp\Client;
$client = new Client();

$response = $client->get('http://116.0.2.210:8080/sanata-api/public/api/resep/take-resep?no_resep='.$_GET['no_resep']);
$data = $response->json();
//  print_r($data['data']['pasien']['no_bukti']);
    include "phpqrcode/qrlib.php"; 

    $tempdir = "temp/"; //Nama folder tempat menyimpan file qrcode
    if (!file_exists($tempdir)) //Buat folder bername temp
        mkdir($tempdir);

        //isi qrcode jika di scan
        $codeContents = 'http://192.168.1.239/project-biling/Resep/'.$_GET['no_resep'].".pdf"; 
    
    //simpan file kedalam temp 
    //nilai konfigurasi Frame di bawah 4 tidak direkomendasikan    
    QRcode::png($codeContents, $tempdir.'008_4.png', QR_ECLEVEL_L, 2, 1); 

        // echo '<h2>Ukuran Frame QRCode</h2>';
        // // displaying 
        // echo '<img src="'.$tempdir.'008_6.png" />';  
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- <style>
        body {
            height: 842px;
            width: 595px;
            /* to centre page on screen*/
            margin-left: auto;
            margin-right: auto;
        } -->
    </style>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="e-resep/style\.css">
    <link href="https://www.dafontfree.net/embed/c3Vuc2V0LXNlcmlhbC1saWdodC1yZWd1bGFyJmRhdGEvMzYvcy8xMjc4MDIvc3Vuc2V0LXNlcmlhbC1saWdodC1yZWd1bGFyLnR0Zg" rel="stylesheet" type="text/css"/>
    <title> E-RESEP </title>
</head>
<body>

    <section class="home bd-grid" id="home">
        <div class="container">
            <img src="e-resep/download.png" style="float: left; width: 58px; height: 60px; margin-left:-10px; margin-top:9px;">
            <h1 style="font-family: 'sunset-serial-light-regular', sans-serif; font-size: 30px; color: rgb(86, 86, 248); ">&nbsp;RSIA.&nbsp;Puri&nbsp;Bunda</h1>
            <h3 style="font-family: 'Didact Gothic', sans-serif; font-size: 16.5px; color: rgb(86, 86, 248);">&nbsp;&nbsp;I N S T A L A S I &nbsp;&nbsp; F A R M A S I</h3>
            <!-- <h5>Jl.Gatot Subroto VI/19 Denpasar,Bali Telp.(0361)437999</h5>
            <h4>Berusaha Menjadi Yang Terbaik</h4> -->
        </div>
        <!-- &nbsp;&nbsp;&nbsp; -->
        <br>
        <div>
            &nbsp;
        <h4 style="font-family:'Courier New', Courier, monospace; font-size: 14px; color: rgb(86, 86, 248); margin-left:40px;">&nbsp;Berusaha Menjadi Yang Terbaik</h4>
        &nbsp;<br>
        <h5 style="font-family: 'IBM Plex Sans Hebrew', sans-serif; font-size:12px; color: rgb(86, 86, 248); margin-top:10px; margin-left:-10px; ">Jl.Gatot Subroto VI/19 Denpasar,Bali Telp.(0361)437999</h5>
        <br><hr style="width: 323px; margin-left: -20px;">
        </div>
        <br>
        <div>
    </div>
    <div>
    <br><br><br><br>
    <h2 style="margin-left: 65px; margin-top:-3px; font-family:'Didact Gothic', sans-serif; font-size:15px; color: rgb(86, 86, 248);">S A L I N A N &nbsp; R E S E P</h2>
    &nbsp;
    <table>
            <tr>
                <td style="font-family:sans-serif; color: rgb(86, 86, 248); font-size:12px;">TGL.</td>
                <td style="font-size:15px;"> <?php echo $data['data']['pasien']['tgl_resep'] ?><p style="border-bottom: 1px dashed #000; margin-top:1px;"></td>
            </tr>
            <tr>
                <td style="font-family:sans-serif; color: rgb(86, 86, 248); font-size:12px;">UNTUK</td>
                <td style="font-size:15px;"> <?php echo $data['data']['pasien']['nama_pasien'] ?><p  style="border-bottom: 1px dashed #000; margin-top:1px;"</td>
            </tr>
            <tr>
                <td style="font-family:sans-serif; color: rgb(86, 86, 248); font-size:12px;">DARI Dr.&nbsp;&nbsp;&nbsp;</td>
                <td style="font-size:15px;"> <?php echo $data['data']['pasien']['nama_dokter'] ?><p  style="border-bottom: 1px dashed #000; margin-top:1px;"></td>
            </tr>
        </table>
    &nbsp;&nbsp;

    <table class="tabel3" border="0" width="300px" >
    <?php
        foreach ($data['data']['resep'] as $resep){
        ?>
            <tr>
                <td style="font-family:sans-serif; font-size:10px;"><?php echo $resep['Nama_Barang']; ?></td>
                <td style="font-family:sans-serif; font-size:10px;"><?php echo $resep['Satuan_Stok']; ?></td>
                <td style="font-family:sans-serif; font-size:10px;"><?php echo $resep['Qty']; ?></td>
                <td style="font-family:sans-serif; font-size:10px;"><?php echo $resep['Dosis'] ?></td>
                 <!-- <td style="font-family:sans-serif; font-size:10px;"><?php echo $resep['Harga_Satuan']; ?></td> -->
            </tr>
            <?php
        }
        ?>
    </table> 
    </div>
    <br>
    <div class="bawah">
    <!-- <br><br><br><br><br><br><br><br><br><br><br>&nbsp;&nbsp;
    <br><br><br><br>&nbsp; -->
        <p style="margin-left:225px; margin-top:359px;">
        <?php
         echo '<img src="'.$tempdir.'008_4.png" />';  
        ?>
        </p>
        <p style="margin-left:235px; font-family:sans-serif; color: rgb(86, 86, 248); font-size:11px;">P.C.C</p>&nbsp;
        <p style="margin-left:235px; font-family:sans-serif; color: rgb(86, 86, 248); font-size:11px;" ><?php echo $_SESSION['nama'];?>
    </div>
</body>
</html>