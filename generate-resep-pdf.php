<?php
include 'koneksi.php';
require_once 'vendor/dompdf/autoload.inc.php';
use Dompdf\Dompdf;
use Dompdf\Options;
$no_resep = $_GET['no_resep'];
// instantiate and use the dompdf class
$options = new Options();
$options->setIsPhpEnabled('true');
$dompdf = new Dompdf($options);

$dompdf->set_option('isPhpEnabled', true);
ob_start();
require 'detailresep.php';

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A6', 'portrait');
// Render the HTML as PDF
$dompdf->loadHtml(ob_get_clean());
$dompdf->render();
$output = $dompdf->output();
file_put_contents("Resep/".$_GET['no_resep'].".pdf", $output);

// Output the generated PDF to Browser
$dompdf->stream('E-Resep'.'pdf', array("Attachment"=>0));

$sql    = "SELECT * FROM reseppdf WHERE no_resep= '$no_resep'";
$db     = mysqli_query($connect, $sql);
$data	= mysqli_fetch_array($db);

$rowcount   = mysqli_num_rows($db);

// pengecekan
if ($rowcount > 0) {
}else{ 
    $query = mysqli_query($connect, "INSERT INTO reseppdf(no_resep) value ('$no_resep')");
    $path="Resep/".$_GET['no_resep'];

} 
?>
