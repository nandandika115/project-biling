<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/email.css">
    <link rel="icon" href="images/fevicon/logo.png" type="image/png" />
    <title></title>
</head>
<body>
<form>
<?php
SESSION_START();
include 'koneksi.php';
require 'config/index.php';
use GuzzleHttp\Client;
$client = new Client();
$no_resep='';
if(isset($_GET['no_resep'])){
    $no_resep = $_GET['no_resep'];
}
$response = $client->get('http://116.0.2.210:8080/sanata-api/public/api/resep/take-resep?no_resep='.$no_resep);
$data = $response->json();
$reservasi = $data['data'];
// var_dump($reservasi);


// print_r($reservasi);
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
// use PHPMailer\PHPMailer;
// use PHPMailer\SMTP;
// use PHPMailer\Exception;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php'; 

// Load Composer's autoloader
require 'vendor/autoload.php';
 
if(isset($_POST))
{
    // Instantiation and passing `true` enables exceptions
    $mail = new PHPMailer(true);
 
    // $no_bukti         = $_POST['no_bukti'];
    // $nama_pengirim      = $_POST['nama_pengirim'];
     $email              = $_POST['email'];
    //  var_dump($email);
    // $nama_file          = $_POST['nama_file'];
 
    //Server settings
    // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                       // Enable verbose debug output
    $mail->isSMTP();     
    // $mail->SMTPDebug = 2;     
    $mail->Debugoutput = 'html';                                  // Send using SMTP
    $mail->Host       = 'smtp.gmail.com';                        // Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    $mail->Username   = 'nandandika117@gmail.com';             // SMTP username
    $mail->Password   = 'arjuna2019';
    $letak_file = "Resep/".$no_resep.'.pdf';                 // SMTP password
    $nama_file   = $no_resep.'.pdf';
    // $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
 
    //Recipients
    $mail->setFrom('nandandika117@gmail.com');
    $mail->addAddress($email);     // Add a recipient
   
    // $mail->addReplyTo('dewa.ariandy@gmail.com');
    // $mail->addCC('cc@example.com');
    // $mail->addBCC('bcc@example.com');
 
    // Attachments
     $mail->AddAttachment($letak_file, $nama_file);
    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
 
    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Konfirmasi Bukti RESEP';
    $mail->Body    = '<p>Yth. Ibu/Bapak</p><p>Salam sehat dari kami<br>
    Dengan Hormat
    Melalui surat elektronik ini kami dari RSIA Puri Bunda ingin memberikan bukti E-RESEP<br>
    Demikian permohonan kami, atas perhatian dan kesempatan yang diberikan kami ucapkan terima kasih.<br>
    Salam Hangat dari RSIA Puri Bunda.</p> ';  
    // $mail->nama_file = $nama_file;
    $pengirim = $_SESSION['user_id'];
    $tangggal_kirim1 = date('Y-m-d');  
 
    if($mail->send())  {
        $query = mysqli_query($connect, "UPDATE reseppdf SET
        status= 1,
        pengirim = '$pengirim',
        tanggal_kirim1 = '$tangggal_kirim1'
        WHERE no_resep = '$no_resep' ")or die (mysqli_error($connect));
        echo '✔ E-Mail Berhasil Di Kirim
        <br><center><button><a href="pdfresep.php?no_resep='.$no_resep.'">OK</a></button></center>';

    }else{
        
        echo '❌ Maaf E-Mail Yang Anda Masukkan Salah {$mail->ErrorInfo}<br>
        <center><button><a href="pdfresep.php?no_resep='.$no_resep.'">COBA KEMBALI</a></button></center>';
    }
}
else{
    echo "Maaf E-Mail Gagal dikirim";
}
?>
</form>
</body>
</html>
