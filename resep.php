<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="style/biling1.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="vendor/boostrap-datepicker/css/bootstrap-datepicker.min.css">
    <title></title>
</head>
<body>
<?php

require 'config/index.php';
use GuzzleHttp\Client;
$client = new Client();
$search='';
if(isset($_GET['search'])){
    $search = $_GET['search'];
}

$no_resep='';
if(isset($_GET['no_resep'])){
    $no_resep = $_GET['no_resep'];
}
$nrm='';
if(isset($_GET['nrm'])){
    $nrm = $_GET['nrm'];
}

$start_date = '';
if(isset($_GET['start_date'])){
    $start_date = $_GET['start_date'];
}

$end_date = '';
if(isset($_GET['end_date'])){
    $end_date = $_GET['end_date'];
}

$next = '';
if(isset($_GET['next'])){
    $next = $_GET['next'];
}
$response = $client->get('http://116.0.2.210:8080/sanata-api/public/api/resep/get-resep?&nama='.$search.'&start='.$start_date.'&end='.$end_date. '&page='.$next.'&no_resep='.$no_resep. '&nrm='.$nrm);
$data = $response->json();
$reservasi = $data['data'];
// print_r($data['data']['data']);

?>
<form action="sign_resep.php" type="GET" class="form-inline ">
  <input style="display:none" value="resep" name="type">
  <input class="search" value="<?=isset($_GET['search'])?$_GET['search']:''?>" type="text" name="search" placeholder="Search">
  <input class="search" value="<?=isset($_GET['no_resep'])?$_GET['no_resep']:''?>" type="text" name=" no_resep" placeholder="Search no resep">
  <input class="search" value="<?=isset($_GET['nrm'])?$_GET['nrm']:''?>" type="text" name=" nrm" placeholder="Search nrm">	
  <div class="input-group col-md-6 date">
    <input type="text" class="form-control datepicker" value="<?= isset($_GET['start_date'])?$_GET['start_date']:''?>" name="start_date" placeholder="Start Date">
    <input type="text" class="form-control datepicker" value="<?= isset($_GET['end_date'])?$_GET['end_date']:''?>" name="end_date" placeholder="End Date">
    <div class="input-group-prepend">
        <span class="glyphicon glyphicon-th"></span>
    </div>
</div>
<button class="btn btn-light" type="submit" value="Cari">Cari</button>			
</form>
<form action="sign_resep.php" type="GET" class="form-inline">
<nav aria-label="Page navigation example">
  <ul class="pagination">
    <li><button type="submit" class="btn btn-primary" value="<?= isset($_GET['page'])?$_GET['page']-1:1?>" <?php if($data['data']['from']==@$_GET['page']){ print_r('disabled');} ?> name="page" placeholder="Previous">Previous</button> &nbsp; <!-- fungsi @ pada php adalah untuk pengganti tag isset --></li>
    <li><input disabled style="width:80px;" value="<?= isset($_GET['page'])?$_GET['page']+0:1?>" <?php if($data['data']['from']==@$_GET['page']){ print_r($data['data']['from']); } ?> ></li> &nbsp;
    <li><button type="submit" class="btn btn-primary" value="<?= isset($_GET['page'])?$_GET['page']+1:1?>" <?php if($data['data']['last_page']==@$_GET['page']){ print_r('disabled');} ?> name="page" placeholder="Next">Next</button></li>
  </ul>
</nav>
</form>
<?php

echo "<table class='table' style=width:100% border=1>";
echo "
    <thead>
        <tr>
            <td>No Resep</td>
            <td>No Registrasi</td>
            <td>Nama Pasien</td>
            <td>Tanggal</td>
            <td>NRM</td>
            <td>Jam</td> 
        </tr>
    <thead>
        ";
    foreach($reservasi['data'] as $data){
    
    echo "
    <tfoot>
        <tr>
            <td><a href='pdfresep.php?no_resep=".$data['NoResep']."'>{$data['NoResep']}</a></td>
            <td>{$data['NoRegistrasi']}</td>
            <td>{$data['NamaPasien_Reg']}</td>
            <td>{$data['Tanggal']}</td>
            <td>{$data['NRM']}</td>
            <td>{$data['Jam']}</td>
        <tr>
    </tfoot>  
    ";
        
}
echo "</table>";
?>
</body>
<script src="vendor/boostrap-datepicker/js/bootstrap-datepicker.min.js"></script>
</html>
