-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2022 at 08:59 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pegawai`
--

-- --------------------------------------------------------

--
-- Table structure for table `reseppdf`
--

CREATE TABLE `reseppdf` (
  `no_resep` varchar(50) NOT NULL,
  `pengirim` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `reseppdf_id` int(11) NOT NULL,
  `tanggal_kirim1` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reseppdf`
--

INSERT INTO `reseppdf` (`no_resep`, `pengirim`, `status`, `reseppdf_id`, `tanggal_kirim1`) VALUES
('211006RSP-022688', 1, 1, 1, '2022-05-03'),
('211006RSP-022691', 1, 1, 2, '2022-05-03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `reseppdf`
--
ALTER TABLE `reseppdf`
  ADD PRIMARY KEY (`reseppdf_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `reseppdf`
--
ALTER TABLE `reseppdf`
  MODIFY `reseppdf_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
